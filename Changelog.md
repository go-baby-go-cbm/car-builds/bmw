# Changelog

----------------------------------------------------------------------------------------------------
## v2.2 [2022-06-10 17:30:00]

### Added
-   Project README.


----------------------------------------------------------------------------------------------------
## v2.1 [2022-06-09 18:10:00]

### Changed
-   Modified the "Build Instructions" so they no longer refer to broken links that were relevant 
    for the build back in 2019 but are no longer active/present.


----------------------------------------------------------------------------------------------------
## v2.0 [2019-05-31 17:00:00]

### Added
-   STL files for 3D printed parts used in the update build.
-   Supplemental pictures covering how to build an example car.
-   Supplemental pictures covering how to build a number of the wiring harnesses.

### Changed
-   Completely overhauled instructions and methods for the June 1st, 2019 build.
-   Updated photo for the finished car used in the README.

### Deleted
-   Previous supplemental files as no longer relevant.


----------------------------------------------------------------------------------------------------
## v1.3 [2018-06-08 17:00:00]

### Changed
-   Updated user guide instructions for the June 9th, 2018 build.


----------------------------------------------------------------------------------------------------
## v1.2 [2017-06-09 17:00:00]

### Changed
-   Updated user guide instructions for the June 10th, 2017 build.


----------------------------------------------------------------------------------------------------
## v1.1 [2016-12-11 21:00:00]

### Added
-   Installation instructions for installing the "Go Button" into the center of the steering wheel.
-   This technique comes from the build on December 12th, 2016.


----------------------------------------------------------------------------------------------------
## v1.0 [2016-07-29 17:00:00]

### Added
-   All materials prepared for the the July 30th, 2016 build.


