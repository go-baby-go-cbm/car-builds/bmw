# STL files


## Overview

This directory contains all STL files that are relevant for the current version of this guide.


## Materials Included

1.  button--adapter-plate.stl
    -   Provides mount points for zip ties for the "Go Button".
    -   Allows securing the "Go Button" to the steering wheel.

2.  dash-mount--fill-unused-mounting-hole.stl
    -   Cover to fill in the unused mounting hole on the dash mount for the "Go Button".
    -   The dash mount can be mounted on the left or right side of the dash and thus has two routing 
        holes. This fills in the unused routing hole.

3.  dash-mount--wire-hole.stl
    -   Cover for the wire hole on the dash mount for the "Go Button".
    -   The wire hole is the hole for which the "Go Button" wires thread through.

4.  foot-pedal--hinge-spacer.stl
    -   Spacer to ensure the screws used to attach the foot pedal design do not stick through the 
        bottom of the car too far (reduce sharp object risk).

5.  foot-pedal--stop.stl
    -   Brace to prevent the foot pedal from swinging too far.

6.  kill-switch-wire-clamp--v2.stl
    -   Secures the kill switch harness to the bottom of the car so that it does not drag on the floor.


