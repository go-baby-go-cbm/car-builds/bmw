# Car Build - BMW

![Finished Car](supplemental/finished-car.jpg)

## Overview

This repository contains materials for building a Go Baby Go car using a BWM model car. 

By following this guide, you will replace the default foot pedal of the car with an actuator ("Go 
Button") that is more suitable for those with mobility issues. There are several mounting options 
included for the "Go Button".


## How to Use this Project

Use the guides and any supplemental materials provided to customize a base BMW model car for your 
Go Baby Go car build. You are encouraged to expand upon the instructions here and to build the car 
that best fits your child's needs.


## What this Project Does Not Cover

1.  How to assemble the car. Please follow the instructions provided with your vehicle for guidance 
    in that regard.

2.  This guide covers the electrical customization and does not address the clinical fitting that 
    will be required to adapt the vehicle for your child's needs. In the future, this may be 
    expanded but presently that is very customized for each child and done so on the fly.

3.  This guide does not have specific instructions for how to make the harnesses used. Please see 
    the supplemental folder for photos that should help in that regard.


## Materials Included

-   [3D Printed Parts](3d-printed-parts)
    -   STL files to 3D print when fabricating your car.

-   [Supplemental](supplemental)
    -   Provides photos and notes that may be helpful while building your car.

-   [User Guides](user-guides)
    -   Guides provide instructions for fabricating your custom Go Baby Go car.


## Version Information

This is version 2.2 of this guide. Please see the [Changelog](Changelog.md) for a detailed version 
history.


## License

This project and materials are released under the [Unlicense](LICENSE). We provide the materials 
included here as a reference to anyone working in the Go Baby Go space. We hope this serves as a 
stepping stone to improved designs and would love to hear back on any improvements made on what we 
have created here.


